import time

def search(pat, txt):
    M = len(pat)
    N = len(txt)
    d = 128
    q = 7671453397  # A prime number
    j = 0
    p = 0  # hash value for pattern
    t = 0  # hash value for txt
    h = 1
    kk = 0

    # h будет выглядеть pow(d, M-1)% q
    for i in range(M - 1):
        h = (h * d) % q

    for i in range(M):
        p = (d * p + ord(pat[i])) % q
        t = (d * t + ord(txt[i])) % q

    for i in range(N - M + 1):
        if p == t:
            for j in range(M):
                if txt[i + j] != pat[j]:
                    break

            j += 1
            if j == M:
                kk += 1

        if i < N - M:
            t = (d * (t - ord(txt[i]) * h) + ord(txt[i + M])) % q

            if t < 0:
                t = t + q

    return kk


with open('test_2.txt', 'r') as f:
    txt = f.read()
pat = "zxc"

start_time = time.time()

print(search(pat, txt))
print("--- %s seconds ---" % (time.time() - start_time))
